## Pour l'ajout du chemin courant le path pour le require
$:.unshift File.join(File.dirname(__FILE__), '..')

require "../http.rb"


describe "HTTP::Response" do
  context "when was created" do
    res = HTTP::Response.new
    it "all attributes are nil" do
      res.body.should be_empty
      res.code.should be_nil
      res.code_message.should be_nil
      res.headers.should be_empty
    end
  end
  context "and some values affected" do
    res = HTTP::Response.new
    res.code = "200"
    res.code_message = "OK"
    res.body = "BODY"
    res.headers["test"]="TRUE"
    it "check" do
      res.code.should == "200"
      res.code_message.should == "OK"
      res.body.should == "BODY"
      res.headers["test"].should == "TRUE"
    end
  end
  context "test methode write" do
    res = HTTP::Response.new
    res.write "test body"
    res.write "test2"
    it "body must contain test body" do
      res.body.join("\n").to_s.should == "test body\ntest2"
    end
  end
  context "test methode to_s" do
    res = HTTP::Response.new
    res.code = "200"
    res.code_message = "OK"
    res.body = ["BODY"]
    res.headers["test"]="TRUE"
    res.headers["test2"]="FALSE"
    it "must rerun the response message with the body" do
      res.to_s.should == "HTTP/1.0 200 OK\ntest: TRUE\ntest2: FALSE\nContent-Length: 4\nContent-Type: text/plain\n\nBODY"
    end
  end
end

