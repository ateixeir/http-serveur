## Pour l'ajout du chemin courant le path pour le require
$:.unshift File.join(File.dirname(__FILE__), '..')

require "stringio"
require "../http.rb"

describe "HTTP::Request" do
  context "when was created" do
    # Dans ces tests le socket sera représenté par une StringIO
    socket = StringIO.new
    req = HTTP::Request.new(socket)
    it "must have 1 attibute for the socket" do
      req.socket.should == socket
    end
  end
  context "when has receive a request in the socket" do
    socket = StringIO.new
    socket.write("GET /page.html HTTP/1.0\nHost: example.com\nReferer: http://example.com/\nUser-Agent: CERN-LineMode/2.15 libwww/2.17b3\nContent-Length: 4\n\nBODY")
    req = HTTP::Request.new(socket)
    it "must have fill header table" do
      req.headers["Host"].should == "example.com"
      req.headers["Referer"].should == "http://example.com/"
      req.headers["User-Agent"].should == "CERN-LineMode/2.15 libwww/2.17b3"
      req.headers["Content-Length"].should == "4"
    end
    it "and the path must be /page.html" do
      req.path.should == "/page.html"
    end
  end  
end
