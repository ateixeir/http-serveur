## Fichier Module de HTTP
module HTTP

  class Request
    attr_accessor :socket, :headers, :body, :path
    def initialize(socket)
      @socket = socket
      @socket.pos=0
      if !(@socket.nil?) 
        # request parsing
        if (request = @socket.gets)
          request = request.chomp
          @path = request.split(" ") [1]
        end
        @headers = {}
        begin
          header = @socket.gets
          if !(header.nil?)
            header = header.chomp
            header_name, header_val = header.split(': ')
            @headers[header_name] = header_val
          end
        end until header.nil?
      
        @body = nil
        unless @headers["Content-Length"].nil?
          @body = @socket.read(@headers["Content-Length"].to_i)
        end
      end
    end
  end


  
  
  class Response
    attr_accessor :code, :code_message, :body, :headers
    def initialize
      @headers = Hash.new
      @body = []
    end
    def write(string)
      @body.concat(["#{string.to_s}"])
    end
    def to_s
      response_codemsg = "HTTP/1.0 #{@code} #{@code_message}\n"
      response_body = @body
      response_headers = @headers
      res_head = String.new
      
      response_body = response_body.join("\n")
      response_headers["Content-Length"] = "#{response_body.length}"
      response_headers["Content-Type"] = "text/plain"

      response_headers.each do |key,val|
        res_head += "#{key}: #{val}\n"
      end 
      res = "#{response_codemsg}#{res_head}\n#{response_body}"
    end
  end
end